// authStore.js
import { writable } from "svelte/store";

// Initialize the auth store with an initial value of false
export const auth = writable(false);
// for saving names 
export const name = writable("");
